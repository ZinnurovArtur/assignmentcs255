package sample;
//change here

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import sample.Directions;

import javax.swing.event.ChangeEvent;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.*;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

import static java.lang.Integer.max;


/**
 * Controller class with algorithms
 *
 * @author Arthur Zinnurov 690380
 * @version 2.3
 */
public class Controller implements Initializable, ChangeListener<Object> {
    @FXML
    private Slider zSlider;
    @FXML
    private Slider ySlider;
    @FXML
    private Slider xSlider;
    @FXML
    private Button mipButton;
    @FXML
    private ImageView zView;
    @FXML
    private ImageView yView;
    @FXML
    private ImageView xView;
    @FXML
    private Button resetButton;
    @FXML
    private Button histogrammButton;


    private static WritableImage medic;


    short cthead[][][]; //store the 3D volume data set
    short min, max; //min/max value in the 3D volume data set
    private float col;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resetButton.setDisable(true);

        ySlider.valueProperty().addListener(this);
        xSlider.valueProperty().addListener(this);
        zSlider.valueProperty().addListener(this);

        yView.setOnMouseClicked(this::resizeControllercall);
        zView.setOnMouseClicked(this::resizeControllercall);
        xView.setOnMouseClicked(this::resizeControllercall);


        try {
            ReadData();
        } catch (IOException e) {

        }
    }


    /**
     * Method for gui to call mip Button
     *
     * @param event
     * @throws IOException
     */
    public void pushMipbutton(ActionEvent event) throws IOException {

        ySlider.valueProperty().removeListener(this);
        xSlider.valueProperty().removeListener(this);
        zSlider.valueProperty().removeListener(this);
        mipButton.setDisable(true);
        zView((int) zSlider.getValue());
        yView((int) ySlider.getValue());
        xView((int) xSlider.getValue());
        resetButton.setDisable(false);
        event.consume();


    }

    /**
     * Button to call histogram equalisation in gui
     *
     * @param event
     * @throws IOException
     */

    public void histogrmaMode(ActionEvent event) throws IOException {

        zView((int) zSlider.getValue());
        yView((int) ySlider.getValue());
        xView((int) xSlider.getValue());

        histogrammButton.setDisable(true);
        resetButton.setDisable(false);
        event.consume();

    }

    /**
     * Method to call reset button in gui with extra validation
     *
     * @param event
     * @throws IOException
     */
    public void resetButton(ActionEvent event) throws IOException {
        if (mipButton.isDisabled()) {
            ySlider.valueProperty().addListener(this);
            xSlider.valueProperty().addListener(this);
            zSlider.valueProperty().addListener(this);

        }
        mipButton.setDisable(false);
        zView((int) zSlider.getValue());
        yView((int) ySlider.getValue());
        xView((int) xSlider.getValue());
        resetButton.setDisable(true);
        histogrammButton.setDisable(false);
        event.consume();

    }


    @Override
    public void changed(ObservableValue<? extends Object> observableValue, Object oldVal, Object newVal) {
        // System.out.println(zSlider.getValue());
        zView((int) zSlider.getValue());
        yView((int) ySlider.getValue());
        xView((int) xSlider.getValue());
        //  System.out.println(xSlider.getValue());
        //  System.out.println(ySlider.getValue());
    }

    /**
     * Read dataset provided from example
     *
     * @throws IOException
     */
    public void ReadData() throws IOException {
        //File name is hardcoded here - much nicer to have a dialog to select it and capture the size from the user
        File file = new File("Path to put ");
        //"C:\Users\Arthur\IdeaProjects\assignmentcs255\src\sample\CThead"
        ///Users/arturzinnurov/IdeaProjects/assignmentcs255/src/sample/CThead
        //Read the data quickly via a buffer (in C++ you can just do a single fread - I couldn't find if there is an equivalent in Java)
        DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));

        int i, j, k; //loop through the 3D data set

        min = Short.MAX_VALUE;
        max = Short.MIN_VALUE; //set to extreme values
        short read; //value read in
        int b1, b2; //data is wrong Endian (check wikipedia) for Java so we need to swap the bytes around

        cthead = new short[113][256][256]; //allocate the memory - note this is fixed for this data set
        //loop through the data reading it in
        for (k = 0; k < 113; k++) {
            for (j = 0; j < 256; j++) {
                for (i = 0; i < 256; i++) {
                    //because the Endianess is wrong, it needs to be read byte at a time and swapped
                    b1 = ((int) in.readByte()) & 0xff; //the 0xff is because Java does not have unsigned types
                    b2 = ((int) in.readByte()) & 0xff; //the 0xff is because Java does not have unsigned types
                    read = (short) ((b2 << 8) | b1); //and swizzle the bytes around
                    if (read < min) min = read; //update the minimum
                    if (read > max) max = read; //update the maximum
                    cthead[k][j][i] = read; //put the short into memory (in C++ you can replace all this code with one fread)
                }
            }
        }
        System.out.println(min + " " + max); //diagnostic - for CThead this should be -1117, 2248
        //(i.e. there are 3366 levels of grey (we are trying to display on 256 levels of grey)
        //therefore histogram equalization would be a good thing
    }

    /**
     * Draw the Zview(top) picture on canvas
     *
     * @param z integer
     */
    public void zView(int z) {
        WritableImage medical_image = new WritableImage(256, 256);

        zView.setImage(medical_image);
        if (!mipButton.isDisabled()) {
            if (histogrammButton.isDisabled()) {

                medical_image = histogramEq(medical_image, z, Directions.Z_AXIS, cudFMap(histoGram(cthead)));
                zView.setImage(medical_image);
            } else {
                sLices(medical_image, z, Directions.Z_AXIS);
            }


            if (zSlider.isValueChanging()) {
                medic = medical_image;
            }

        } else {
            medical_image = MIP(medical_image, z, Directions.Z_AXIS);
            zView.setImage(medical_image);

        }
    }

    /**
     * Draw the front picture on canvas
     *
     * @param y integer
     */
    public void yView(int y) {
        WritableImage medical_image = new WritableImage(256, 112);


        yView.setImage(medical_image);
        if (!mipButton.isDisabled()) {
            if (histogrammButton.isDisabled()) {

                medical_image = histogramEq(medical_image, y, Directions.Y_AXIS, cudFMap(histoGram(cthead)));
                yView.setImage(medical_image);
            } else {
                sLices(medical_image, y, Directions.Y_AXIS);
            }
            if (ySlider.isValueChanging()) {
                medic = medical_image;
            }


        } else {
            MIP(medical_image, y, Directions.Y_AXIS);

        }

    }

    /**
     * Draw the side view picture on canvas
     *
     * @param x
     */
    public void xView(int x) {
        WritableImage medical_image = new WritableImage(256, 112);

        xView.setImage(medical_image);
        if (!mipButton.isDisabled()) {

            if (histogrammButton.isDisabled()) {

                medical_image = histogramEq(medical_image, x, Directions.X_AXIS, cudFMap(histoGram(cthead)));
                xView.setImage(medical_image);
            } else {
                sLices(medical_image, x, Directions.X_AXIS);
            }
            if (xSlider.isValueChanging()) {
                medic = medical_image;
            }
        } else {
            MIP(medical_image, x, Directions.X_AXIS);
        }
    }

    /**
     * Create MIP picture of dataset
     *
     * @param image
     * @param value     from slider
     * @param direction which type of view
     * @return new picture
     */
    public WritableImage MIP(WritableImage image, int value, Directions direction) {
        //Get image dimensions, and declare loop variables
        int w = (int) image.getWidth(), h = (int) image.getHeight(), i, j, c, k;
        PixelWriter image_writer = image.getPixelWriter();


        short datum;
        for (j = 0; j < h; j++) {
            for (i = 0; i < w; i++) {

                switch (direction) {
                    case X_AXIS:
                        datum = cthead[j][i][value];
                        for (int p = 0; p <= 255; p++) {
                            datum = (short) max(cthead[j][i][p], datum);
                        }
                        col = (((float) datum - (float) min) / ((float) (max - min)));
                        break;

                    case Y_AXIS:
                        datum = cthead[j][value][i];
                        for (int p = 0; p <= 255; p++) {
                            datum = (short) max(cthead[j][p][i], datum);
                        }
                        col = (((float) datum - (float) min) / ((float) (max - min)));
                        break;
                    case Z_AXIS:
                        datum = cthead[value][j][i];
                        for (int p = 0; p <= 112; p++) {
                            datum = (short) max(cthead[p][j][i], datum);
                        }
                        col = (((float) datum - (float) min) / ((float) (max - min)));

                        break;
                }
                for (c = 0; c < 3; c++) {
                    //and now we are looping through the bgr components of the pixel
                    //set the colour component c of pixel (i,j)
                    image_writer.setColor(i, j, Color.color(col, col, col, 1.0));


                    //  data[c+3*i+3*j*w]=(byte) col;
                } // colour loop
            } // column loop
        }
        return image;// row loop


    }

    /**
     * Go through each slice
     *
     * @param image
     * @param value     from the slider
     * @param direction which type of the view
     */
    public void sLices(WritableImage image, int value, Directions direction) {
        //Get image dimensions, and declare loop variables
        int w = (int) image.getWidth(), h = (int) image.getHeight(), i, j, c, k;
        PixelWriter image_writer = image.getPixelWriter();


        short datum;
        for (j = 0; j < h; j++) {
            for (i = 0; i < w; i++) {

                switch (direction) {
                    case X_AXIS:
                        datum = cthead[j][i][value];
                        col = (((float) datum - (float) min) / ((float) (max - min)));
                        break;

                    case Y_AXIS:
                        datum = cthead[j][value][i];
                        col = (((float) datum - (float) min) / ((float) (max - min)));

                        break;
                    case Z_AXIS:
                        datum = cthead[value][j][i];
                        col = (((float) datum - (float) min) / ((float) (max - min)));
                        break;
                }
                for (c = 0; c < 3; c++) {
                    //and now we are looping through the bgr components of the pixel
                    //set the colour component c of pixel (i,j)

                    image_writer.setColor(i, j, Color.color(col, col, col, 1.0));
                    // alldata[i][j][c] = (short)image.getPixelReader().getArgb(i,j);


                    //  data[c+3*i+3*j*w]=(byte) col;
                } // colour loop
            } // column loop
        }

        // row loop


    }

    /**
     * Creating the histogram of the picture
     *
     * @param data
     * @return array of histogram
     */
    public int[] histoGram(short[][][] data) {
        int[] histogrma;
        histogrma = new int[max - min + 1];
        int index;
        for (int z = 0; z < data.length; z++) {
            for (int i = 0; i < data[z].length; i++) {
                for (int j = 0; j < data[z][i].length; j++) {
                    index = cthead[z][i][j] - min;
                    histogrma[index]++;
                }


            }
        }
        return histogrma;

    }

    /**
     * Cumutative distrubution function with mapping
     *
     * @param histogram array
     * @return array of mapping
     */
    public double[] cudFMap(int[] histogram) {
        int[] t = new int[histogram.length];
        double[] mapping = new double[histogram.length];//generally to 255
        int t_i = 0;

        t[0] = histogram[0];
        for (int i = 1; i < histogram.length; i++) {
            t[i] = t[i - 1] + histogram[i];
            mapping[i] = (((float) t[i] / (float) (113 * 256 * 256)));

        }
        return mapping;
    }

    /**
     * Return image to transfer to another stage
     *
     * @return image
     */
    public WritableImage getMedic() {
        return this.medic;
    }

    /**
     * Draw histogram equalisation using mapping array
     *
     * @param image
     * @param value     from slider
     * @param direction
     * @param mapping   array
     * @return final image
     */
    public WritableImage histogramEq(WritableImage image, int value, Directions direction, double[] mapping) {
        int w = (int) image.getWidth(), h = (int) image.getHeight(), i, j, c;
        PixelWriter image_writer = image.getPixelWriter();


        short datum;
        for (j = 0; j < h; j++) {
            for (i = 0; i < w; i++) {

                switch (direction) {
                    case X_AXIS:
                        datum = cthead[j][i][value];
                        col = (float) mapping[datum - min];
                        break;

                    case Y_AXIS:
                        datum = cthead[j][value][i];
                        col = (float) mapping[datum - min];

                        break;
                    case Z_AXIS:

                        datum = cthead[value][j][i];
                        col = (float) mapping[datum - min];
                        break;
                }
                for (c = 0; c < 3; c++) {
                    image_writer.setColor(i, j, Color.color(col, col, col, 1.0));

                }
            }
        }
        return image;
    }

    /**
     * Call another stage to do resize
     *
     * @param event
     */
    public void resizeControllercall(MouseEvent event) {


        try {
            Parent tableViewParent2 = FXMLLoader.load(getClass().getResource("resize.fxml"));
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            Stage stage = new Stage();
            stage.setTitle("Resize Mode ");
            stage.setScene(new Scene(tableViewParent2, 644, 432));
            window.show();
            stage.show();
        } catch (IOException e) {
            System.exit(-1);

        }

    }


}
