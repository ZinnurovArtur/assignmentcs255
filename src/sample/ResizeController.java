package sample;
//change here

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import javax.imageio.IIOException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller to do resize
 *
 * @author Arthur Zinnurov 690380
 * @version 2.0
 */

public class ResizeController implements Initializable, ChangeListener<Object> {
    @FXML
    private Slider scale;
    @FXML
    private Slider scaleY;
    @FXML
    private Button neirestButton;
    @FXML
    private Button bilenearButton;
    @FXML
    private ImageView view;
    @FXML
    private Label percent;
    @FXML
    private Label perecent2;
    @FXML
    private Button resetBut;

    private FXMLLoader loginLoader = new FXMLLoader(getClass().getResource("gui.fxml"));
    private WritableImage image;
    int sizex;
    int sizey;
    private float col;

    public void initialize(URL location, ResourceBundle resources) {
        try {
            Parent root = this.loginLoader.load();//Must not be deleted
            Controller mainContr = this.loginLoader.getController();
            image = mainContr.getMedic();


        } catch (IOException e) {
            System.exit(-1);
        }

    }

    /**
     * Call resetbutton on gui
     *
     * @param event
     * @throws IOException
     */

    public void resetButtOn(ActionEvent event) throws IOException {
        scale.valueProperty().removeListener(this);
        scaleY.valueProperty().removeListener(this);
        neirestButton.setDisable(false);
        bilenearButton.setDisable(false);
        resetBut.setDisable(true);


    }

    /**
     * call bilinear button  method to do resize
     *
     * @param event
     * @throws IOException
     */
    public void useBilin(ActionEvent event) throws IOException {
        scale.valueProperty().addListener(this);
        scaleY.valueProperty().removeListener(this);
        bilenearButton.setDisable(true);
        resetBut.setDisable(false);
    }

    /**
     * Call Nearest Neighbour button method to do resize
     *
     * @param event
     * @throws IOException
     */
    public void useNNres(ActionEvent event) throws IOException {
        scale.valueProperty().addListener(this);
        scaleY.valueProperty().addListener(this);
        neirestButton.setDisable(true);
        resetBut.setDisable(false);

    }

    @Override
    public void changed(ObservableValue<? extends Object> observableValue, Object oldVal, Object newVal) {
        if (scale.isValueChanging() && neirestButton.isDisable()) {

            percent.setText(((int) scale.getValue()) + "%");
            WritableImage img = new WritableImage((int) scale.getValue(), (int) scale.getValue());
            resizeNeighbour(img, (int) image.getWidth(), (int) scale.getValue(), (int) image.getHeight(), (int) scale.getValue(), imgtoArr(image));
            sizex = (int) scale.getValue();
            sizey = (int) scale.getValue();
            view.setImage(img);
        } else if (scaleY.isValueChanging() && neirestButton.isDisable()) {
            perecent2.setText(((int) scaleY.getValue()) + "%");
            scaleY.setMax(image.getHeight());
            WritableImage img2 = new WritableImage(sizex, sizey);
            resizeNeighbour(img2, (int) scaleY.getValue(), (int) img2.getWidth(), (int) scaleY.getValue(), (int) img2.getHeight(), imgtoArr(image));
            view.setImage(img2);

        } else if (scale.isValueChanging() && bilenearButton.isDisable()) {
            percent.setText(((int) scale.getValue()) + "%");
            WritableImage img = new WritableImage((int) scale.getValue(), (int) scale.getValue());
            resizeBilinear(img, (int) image.getWidth(), (int) scale.getValue(), (int) image.getHeight(), (int) scale.getValue(), imgtoArr(image));
            view.setImage(img);
        }


    }

    /**
     * Algorithm to do resize Neirest Neighbour algorithm
     *
     * @param image
     * @param width   old
     * @param width2  new
     * @param height  old
     * @param height2 new
     * @param data    image converted tp 3d array
     */
    public void resizeNeighbour(WritableImage image, int width, int width2, int height, int height2, short[][][] data) {

        PixelWriter imagetoPixel = image.getPixelWriter();
        short datum[][][] = data;
        short datum3[][][] = new short[width2][height2][4];
        short datum2;
        int max = 255;
        int min = 0;
        float px, py;

        for (int j = 0; j < height2 - 1; j++) {
            for (int i = 0; i < width2 - 1; i++) {
                for (int c = 0; c < 3; c++) {
                    py = j * (height / (float) height2);
                    px = i * (width / (float) width2);
                    datum3[i][j][c] = datum[(int) px][(int) py][c];
                    datum2 = datum3[i][j][c];
                    col = (((float) datum2 - (float) min) / ((float) (max - min)));
                    imagetoPixel.setColor(i, j, Color.color(col, col, col, 1.0));
                }


            }
        }

    }

    /**
     * Linear interpolation method
     *
     * @param a
     * @param b
     * @param c product equation
     * @return
     */
    public float lerp(float a, float b, float c) {

        return a + (b - a) * c;
    }

    /**
     * Actual biliniear interpolation where we look in both sides and y axis
     *
     * @param a00
     * @param a01
     * @param a10
     * @param a11
     * @param x
     * @param y
     * @return
     */
    public float blerp(float a00, float a01, float a10, float a11, float x, float y) {
        return lerp(lerp(a00, a10, x), lerp(a01, a11, x), y);

    }

    /**
     * Perform BI resize and draw on canvas
     *
     * @param image
     * @param width   old
     * @param width2  new
     * @param height  old
     * @param height2 new
     * @param data    3d array of image
     */
    public void resizeBilinear(WritableImage image, int width, int width2, int height, int height2, short[][][] data) {
        short[][][] dataAll = new short[width2][height2][4];
        float xratio = ((float) (width - 1) / width2);
        float yratio = ((float) (height - 1) / height2);
        // Color A,B,C,D;
        int px, py;
        int max = 255;
        int min = 0;
        float xdiff, ydiif;
        for (int i = 0; i < height2 - 1; i++) {
            for (int j = 0; j < width2 - 1; j++) {
                px = (int) (xratio * j);
                py = (int) (yratio * i);
                xdiff = (xratio * j) - px;
                ydiif = (yratio * i) - py;

                dataAll[j + 1][i][1] = data[px + 1][py][1];
                int c00r = dataAll[j + 1][i][1];
                dataAll[j][i + 1][1] = data[px][py + 1][1];
                int c01r = dataAll[j][i + 1][1];
                dataAll[j + 1][i][1] = data[px + 1][py][1];
                int c10r = dataAll[j + 1][i][1];
                dataAll[j + 1][i + 1][1] = data[px + 1][py + 1][1];
                int c11r = dataAll[j + 1][i + 1][1];

                int color = (int) blerp(c00r, c01r, c10r, c11r, xdiff, ydiif);


                col = (((float) color - (float) min) / ((float) (max - min)));

                image.getPixelWriter().setColor(j, i, Color.color(col, col, col, 1.0));

            }
        }

    }

    /**
     * Transform image to 3d array into x,y,color values
     *
     * @param image old
     * @return 3d array
     */
    public short[][][] imgtoArr(WritableImage image) {
        short[][][] data = new short[(int) image.getWidth()][(int) image.getHeight()][4];
        for (int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight(); j++) {
                int px = image.getPixelReader().getArgb(i, j);
                // px binary number gets 24 zeros from left
                int alpha = (px >> 24) & 0xFF;
                int red = (px >> 16) & 0xFF;
                int green = (px >> 8) & 0xFF;
                int blue = px & 0xFF;
                data[i][j][0] = (short) alpha;
                data[i][j][1] = (short) red;
                data[i][j][2] = (short) green;
                data[i][j][3] = (short) blue;

            }

        }
        return data;
    }


}
